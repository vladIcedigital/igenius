import 'dart:ui';
import 'package:flame/gestures.dart';
import 'package:flutter/gestures.dart';
import 'package:flame/game.dart';
import 'package:igenius/components/rectangle.dart';
import 'package:igenius/game.dart';

class Player {
  double x, y, width, height;
  Rect playerRect;
  Paint playerPaint;
  bool dragStarted = false;
  IGeniusGame game;
  List<Rectangle> borders;
  Player(game, borders, x, y, width, height) {
    this.x = x;
    this.y = y;
    this.game = game;
    this.borders = borders;
    this.width = width;
    this.height = height;
    playerRect = Rect.fromLTWH(x - width / 2, y - height / 2, width, height);
    playerPaint = Paint();
    playerPaint.color = Color(0xffff0000);
  }

  void render(canvas) {
    canvas.drawRect(playerRect, playerPaint);
  }

  void onPanDown(DragDownDetails details) {
    if (playerRect.contains(details.globalPosition)) {
      dragStarted = true;
      game.gameStart();
    }
  }

  void onPanUpdate(DragUpdateDetails details) {
    if (dragStarted) {
      playerRect = playerRect.translate(details.delta.dx, details.delta.dy);
      if (borders != null) {
        borders.forEach((element) {
          if (element.playerRect.overlaps(playerRect)) {
            game.gameOver();
          }
        });
      }
    }
  }

  void onPanUp(DragEndDetails details) {
    dragStarted = false;
  }
}
